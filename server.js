const express = require('express');
const session = require('express-session');
const path = require('path');
const routes = require('./routes');

const app = express();

app.use(session({secret: 'rahasia', resave: false, saveUninitialized: true}));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(routes);
app.use(express.static(path.join(__dirname, 'public')));

app.listen(process.env.PORT || 3000);
