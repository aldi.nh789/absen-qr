function submit() {
  const username = $('#username').val();
  const password = $('#password').val();
  $.post('/admin/login', {username, password}, function(hasil) {
    if (hasil.toLowerCase() === 'berhasil') {
      location.href = '/admin';
    } else {
      alert(hasil);
    }
  });
}