$(function() {
  fetchAbsensi();
  fetchPegawai();
});

function fetchAbsensi() {
  $.get('/absensi', function(absensi) {
    const selectAbsensi = document.querySelector('#absensi');
    absensi.forEach(absensi => {
      const {id, jenis, cabang, keterangan} = absensi;
      const {length} = selectAbsensi.options;
      selectAbsensi.options[length] = new Option(`${jenis} | ${cabang} (${keterangan})`, id);
    });
  })
}

function fetchPegawai() {
  $.get('/pegawai', function(pegawai) {
    const selectPegawai = document.querySelector('#pegawai');
    pegawai.forEach(pegawai => {
      const {id, nip, nama} = pegawai;
      const {length} = selectPegawai.options;
      selectPegawai.options[length] = new Option(`${nip ? `(${nip})` : ''} ${nama}`, id);
    });
  })
}

function submit() {
  const tipe = $('#tipe').val();
  const absensi = $('#absensi').val();
  const pegawai = $('#pegawai').val();
  const url = `/absen?tipe=${tipe}&absensi=${absensi}&pegawai=${pegawai}`;
  if (tipe === 'absen') {
    $.get(url, function(pesan) {
      alert(pesan);
    });
  } else {
    location.href = url;
  }
}