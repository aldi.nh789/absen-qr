const state = {
  activeMenu: 'menu-cabang',
  activeNav: 'nav-cabang',
};



/* Cabang */
function menuCabang() {
  if (state.activeMenu !== 'menu-cabang') {
    bukaMenu('menu-cabang');
    aktifkan('nav-cabang');
    tampilCabang();
  }
}
function tampilCabang() {
  const tabel = document.createElement('table');
  const badanTabel = document.createElement('tbody');
  const field = document.querySelector('#data-cabang');
  tabel.className = 'table';
  $.get('/cabang', function(data) {
    tambahBaris(badanTabel, ['Nama'], true);
    data.forEach(function(cabang) {
      tambahBaris(badanTabel, [cabang.nama]);
    });
  });
  while (field.lastChild) {
    field.removeChild(field.lastChild);
  }  
  field.append(tabel);
  tabel.append(badanTabel);
}
function tambahCabang() {
  const nama = $('#cabang-nama').val();
  $.post('/cabang', {nama}, function(data) {
    alert(data);
    tampilCabang();
  })
}



/* Pegawai */
function menuPegawai() {
  if (state.activeMenu !== 'menu-pegawai') {
    bukaMenu('menu-pegawai');
    aktifkan('nav-pegawai');
    tampilPegawai();  
  }
}
function tampilPegawai() {
  const tabel = document.createElement('table');
  const badanTabel = document.createElement('tbody');
  const field = document.querySelector('#data-pegawai');
  tabel.className = 'table';
  $.get('/pegawai', function(data) {
    tambahBaris(badanTabel, ['NIP', 'Nama', 'Jabatan', 'Pangkat'], true);
    data.forEach(function(pegawai) {
      tambahBaris(badanTabel, [pegawai.nip || '', pegawai.nama, pegawai.jabatan, pegawai.pangkat || '']);
    });
  });
  while (field.lastChild) {
    field.removeChild(field.lastChild);
  }  
  field.append(tabel);
  tabel.append(badanTabel);
}
function tambahPegawai() {
  const nama = $('#pegawai-nama').val();
  const tipe_jabatan = $('#pegawai-tipe-jabatan').val();
  const nip = $('#pegawai-nip').val();
  const jabatan = $('#pegawai-jabatan').val();
  const pangkat = $('#pegawai-pangkat').val();
  $.post('/pegawai', {nama, tipe_jabatan, nip, jabatan, pangkat}, function(data) {
    alert(data);
    tampilPegawai();
  });
}
function tampilkanMenuPNS() {
  if ($('#pegawai-tipe-jabatan').val() === 'pns') {
    $('.menu-pns').show();
  } else {
    $('.menu-pns').hide();
  }
}



/* Absensi */
function menuAbsensi() {
  if (state.activeMenu !== '') {
    bukaMenu('menu-absensi');
    aktifkan('nav-absensi');
    tampilAbsensi();
    $.get('/cabang', function(data) {
      const selectCabang = document.querySelector('#absensi-cabang');
      data.forEach(cabang => {
        const {id, nama} = cabang;
        const {length} = selectCabang.options;
        selectCabang.options[length] = new Option(nama, id);
      });
    })  
  }
}
function tampilAbsensi() {
  const tabel = document.createElement('table');
  const badanTabel = document.createElement('tbody');
  const field = document.querySelector('#data-absensi');
  tabel.className = 'table';
  $.get('/absensi', function(data) {
    tambahBaris(badanTabel, ['Cabang', 'Jenis', 'Keterangan'], true);
    data.forEach(function(absensi) {
      tambahBaris(badanTabel, [absensi.cabang, absensi.jenis, absensi.keterangan]);
    });
  });
  while (field.lastChild) {
    field.removeChild(field.lastChild);
  }  
  field.append(tabel);
  tabel.append(badanTabel);
}
function tambahAbsensi() {
  const cabang = $('#absensi-cabang').val();
  const jenis = $('#absensi-jenis').val();
  const keterangan = $('#absensi-keterangan').val();
  $.post('/absensi', {cabang, jenis, keterangan}, function(data) {
    alert(data);
    tampilAbsensi();
  });
}



/* Hari Libur */
function menuHariLibur() {
  if (state.activeMenu !== 'menu-hari-libur') {
    bukaMenu('menu-hari-libur');
    aktifkan('nav-hari-libur');
    tampilHariLibur();
  }
}
function tampilHariLibur() {
  const tabel = document.createElement('table');
  const badanTabel = document.createElement('tbody');
  const field = document.querySelector('#data-hari-libur');
  tabel.className = 'table';
  $.get('/libur', function(data) {
    tambahBaris(badanTabel, ['Tanggal', 'Keterangan'], true);
    data.forEach(function(hariLibur) {
      tambahBaris(badanTabel, [new Date(hariLibur.tanggal).toLocaleDateString(), hariLibur.keterangan]);
    });
  });
  while (field.lastChild) {
    field.removeChild(field.lastChild);
  }  
  field.append(tabel);
  tabel.append(badanTabel);
}
function tambahHariLibur() {
  const tanggal = $('#hari-libur-tanggal').val();
  const keterangan = $('#hari-libur-keterangan').val();
  $.post('/libur', {tanggal, keterangan}, function(data) {
    alert(data);
    tampilHariLibur();
  });
}



/* Perizinan */
function menuPerizinan() {
  if (state.activeMenu !== 'menu-perizinan') {
    bukaMenu('menu-perizinan');
    aktifkan('nav-perizinan');
    tampilPerizinan();
    $.get('/absensi', function(data) {
      const selectAbsensi = document.querySelector('#perizinan-absensi');
      data.forEach(absensi => {
        const {id, jenis, cabang, keterangan} = absensi;
        const {length} = selectAbsensi.options;
        selectAbsensi.options[length] = new Option(`${cabang} | ${jenis} (${keterangan})`, id);
      });
    });
    $.get('/pegawai', function(data) {
      const selectAbsensi = document.querySelector('#perizinan-pegawai');
      data.forEach(pegawai => {
        const {id, nip, nama} = pegawai;
        const {length} = selectAbsensi.options;
        selectAbsensi.options[length] = new Option(`${nama} ${nip ? `(${nip})` : ''}`, id);
      });
    });
  }
}
function tampilPerizinan() {
  const tabel = document.createElement('table');
  const badanTabel = document.createElement('tbody');
  const field = document.querySelector('#data-perizinan');
  tabel.className = 'table';
  $.get('/izin', function(data) {
    tambahBaris(badanTabel, ['Tanggal', 'Keterangan', 'Pegawai', 'Absensi'], true);
    data.forEach(function(izin) {
      tambahBaris(badanTabel, [
        new Date(izin.tanggal).toLocaleDateString(), izin.keterangan, izin.pegawai, izin.absensi
      ]);
    });
  });
  while (field.lastChild) {
    field.removeChild(field.lastChild);
  }  
  field.append(tabel);
  tabel.append(badanTabel);
}
function tambahPerizinan() {
  const tanggal = $('#perizinan-tanggal').val();
  const keterangan = $('#perizinan-keterangan').val();
  const pegawai = $('#perizinan-pegawai').val();
  const absensi = $('#perizinan-absensi').val();
  $.post('/izin', {tanggal, keterangan, pegawai, absensi}, function(data) {
    alert(data);
    tampilPerizinan();
  });
}



/* Laporan */
function menuLaporan() {
  if (state.activeMenu !== 'menu-laporan') {
    bukaMenu('menu-laporan');
    aktifkan('nav-laporan');
    $.get('/absensi', function(data) {
      const selectAbsensi = document.querySelector('#laporan-absensi');
      data.forEach(absensi => {
        const {id, jenis, cabang, keterangan} = absensi;
        const {length} = selectAbsensi.options;
        selectAbsensi.options[length] = new Option(`${cabang} | ${jenis} (${keterangan})`, id);
      });
    });
  }
}



/* Fungsional */
function tambahBaris(badanTabel, data, kepala=false) {
  const baris = document.createElement('tr');
  badanTabel.append(baris);
  data.forEach(text => {
    const kolom = document.createElement(kepala ? 'th' : 'td');
    kolom.append(text);
    baris.append(kolom);
  })
}
function range(from, end) {
  return [...Array(end).keys()].map(i => i + from);
}
function tanggalTerakhirDari(tahun, bulan) {
  return new Date(tahun, bulan, 0).getDate();
}
function bukaMenu(menu) {
  if (menu) {
    $('.' + state.activeMenu).css('display', 'none');
    state.activeMenu = menu;  
  }
  $('.' + state.activeMenu).css('display', 'block');
}
function aktifkan(nav) {
  if (nav) {
    $('#' + state.activeNav).removeClass('active');
    state.activeNav = nav;
  }
  $('#' + state.activeNav).addClass('active');
}



/* Init */
!function init() {
  tampilCabang();
}()
