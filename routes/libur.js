const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const {tahun, bulan} = req.query;

    const nextBulan = String(Number.parseInt(bulan) + 1).padStart(2, '0');
    let query = 'SELECT * FROM hari_libur ';
    if (tahun && bulan) {
      query += `WHERE tanggal<'${tahun}-${nextBulan}-01' AND tanggal>='${tahun}-${bulan}-01'`;
    }
    const {results} = await execQuery(query);

    res.json(results);
  } catch (err) {
    next(err);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const {tanggal, keterangan} = req.body;

    const {results} = await execQuery('SELECT * FROM hari_libur WHERE tanggal=?', [tanggal]);
    const [hari_libur] = results;

    if (hari_libur) {
      if (hari_libur.keterangan !== keterangan) {
        execQuery('UPDATE hari_libur SET keterangan=? WHERE id?', [keterangan, hari_libur.id]);
      }
      res.end('Data Tersebut Terupdate');
    } else {
      execQuery('INSERT INTO hari_libur VALUES(null, ?, ?)', [tanggal, keterangan]);
      res.end('Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
