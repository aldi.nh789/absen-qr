const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', (req, res, next) => {
  if (req.session.loggedIn) {
    next();
  } else {
    res.redirect('/admin/login.html');
  }
});

router.post('/login', async (req, res, next) => {
  const {username, password} = req.body;

  const {results} = await execQuery('SELECT * FROM admin WHERE username=? AND password=?', [username, password]);
  const [admin] = results;

  if (admin) {
    req.session.loggedIn = true;
    res.end('Berhasil');
  } else {
    res.end('Username atau Password Salah');
  }
});

router.get('/logout', async (req, res, next) => {
  req.session.loggedIn = false;
  res.redirect('/admin/login.html');
});

router.get('/:page', async (req, res, next) => {
  const {page} = req.params;
  const {loggedIn} = req.session;

  if (page === 'login.html') {
    if (loggedIn) {
      res.redirect('/admin');
    } else {
      next();
    }
  } else if (loggedIn) {
    next();
  } else {
    res.end('Unauthorized');
  }
});

module.exports = router;
