const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (_req, res, next) => {
  try {
    const {results} = await execQuery('SELECT * FROM pegawai');

    res.json(results);
  } catch (err) {
    next(err);
  }
});
  
router.post('/', async (req, res, next) => {
  try {
    const {nip, nama, tipe_jabatan, jabatan, pangkat} = req.body;
    let insertJabatan, insertPangkat;

    const {results} = await execQuery('SELECT * FROM pegawai WHERE nip=? AND nama=?', [nip, nama]);
    const [pegawai] = results;

    if (tipe_jabatan === 'honorer') {
      insertJabatan = tipe_jabatan;
      insertPangkat = null;
    } else {
      insertJabatan = jabatan;
      insertPangkat = pangkat;
    }

    if (pegawai) {
      if (pegawai.nama !== nama) {
        execQuery('UPDATE pegawai SET nama=?, jabatan=?, pangkat=? WHERE id=?', [nama, insertJabatan, insertPangkat, pegawai.id]);
      }
      res.end('Data dengan nip tersebut sudah ada dan berhasil di Update');
    } else {
      execQuery('INSERT INTO pegawai VALUES (null, ?, ?, ?, ?)', [nip || null, nama, insertPangkat ,insertJabatan]);
      res.end('Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
