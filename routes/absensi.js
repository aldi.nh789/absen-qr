const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (_req, res, next) => {
  try {
    const query = 'SELECT absensi.id, absensi.jenis, absensi.keterangan, cabang.nama AS cabang ' +
      'FROM absensi JOIN cabang ON absensi.id_cabang=cabang.id';
    const {results} = await execQuery(query);

    res.json(results);      
  } catch (err) {
    next(err);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const {cabang, jenis, keterangan} = req.body;

    const query = 'SELECT * FROM absensi WHERE jenis=? AND keterangan=? AND id_cabang=?';
    const {results} = await execQuery(query, [jenis, keterangan, cabang]);
    const [absensi] = results;

    if (absensi) {
      res.end('Absensi tersebut sudah pernah dibuat');
    } else {
      execQuery('INSERT INTO absensi VALUES(null, ?, ?, ?)', [jenis, keterangan, cabang]);
      res.end('Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
