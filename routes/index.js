const router = require('express').Router();

const routerAdmin = require('./admin');
const routerCabang = require('./cabang');
const routerPegawai = require('./pegawai');
const routerAbsensi = require('./absensi');
const routerAbsen = require('./absen');
const routerLaporan = require('./laporan');
const routerHariLibur = require('./libur');
const routerPerizinan = require('./izin');

router.use('/admin', routerAdmin);
router.use('/cabang', routerCabang);
router.use('/pegawai', routerPegawai);
router.use('/absensi', routerAbsensi);
router.use('/absen', routerAbsen);
router.use('/laporan', routerLaporan);
router.use('/libur', routerHariLibur);
router.use('/izin', routerPerizinan);

module.exports = router;
