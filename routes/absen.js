const router = require('express').Router();
const qrcode = require('qrcode');

const {execQuery} = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const {tipe, absensi, pegawai} = req.query;

    if (tipe && tipe === 'qrcode') {
      const host = req.header('host');
      const linkAbsen = `${host}/absen?absensi=${absensi}&pegawai=${pegawai}`;

      const outputQR = await qrcode.toDataURL(linkAbsen);
      return res.end(`<!DOCTYPE html><img src="${outputQR}"/>`);
    }

    const query = 'SELECT * FROM data_absen WHERE tanggal=CURRENT_DATE() AND id_pegawai=? AND id_absensi=?';
    const {results} = await execQuery(query, [pegawai, absensi]);
    const [dataAbsen] = results;

    if (dataAbsen) {
      res.end('Sudah absen hari ini');
    } else {
      execQuery('INSERT INTO data_absen VALUES(null, CURRENT_DATE(), CURRENT_TIME(), ?, ?)', [pegawai, absensi]);
      res.end('Absen Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
