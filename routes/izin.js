const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const query = 'SELECT perizinan.tanggal, perizinan.keterangan, pegawai.nama AS pegawai, absensi.jenis AS absensi ' +
      'FROM perizinan JOIN pegawai ON pegawai.id=perizinan.id_pegawai JOIN absensi ON absensi.id=perizinan.id_absensi';
    const {results} = await execQuery(query);

    res.json(results);
  } catch (err) {
    next(err);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const {tanggal, keterangan, absensi, pegawai} = req.body;

    const query = 'SELECT * FROM perizinan WHERE tanggal=? AND id_absensi=? AND id_pegawai=?';
    const {results} = await execQuery(query, [tanggal, absensi, pegawai]);
    const [perizinan] = results;
  
    if (perizinan) {
      if (perizinan.keterangan !== keterangan) {
        execQuery('UPDATE perizinan SET keterangan=? WHERE id=?', [keterangan, perizinan.id]);
      }
      res.end('Data Terupdate');
    } else {
      execQuery('INSERT INTO perizinan VALUES(null, ?, ?, ?, ?)', [tanggal, keterangan, pegawai, absensi]);
      res.end('Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
