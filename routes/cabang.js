const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (_req, res, next) => {
  try {
    const {results} = await execQuery('SELECT * FROM cabang');

    res.json(results);
  } catch (err) {
    next(err);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const {nama} = req.body;

    if (!nama) {
      return res.end('Nama Cabang Tidak Valid');
    }

    const {results} = await execQuery('SELECT * FROM cabang WHERE nama=?', [nama]);
    const [cabang] = results;

    if (cabang) {
      res.end('Data tersebut sudah pernah ada');
    } else {
      execQuery('INSERT INTO cabang VALUES(null, ?)', [nama]);
      res.end('Berhasil');
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
