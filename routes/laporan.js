const router = require('express').Router();

const {execQuery} = require('../db');

router.get('/', async (req, res, next) => {
  try {
    const {tahun, bulan, absensi} = req.query;

    const nextBulan = String(Number.parseInt(bulan) + 1).padStart(2, '0');
    const queryAbsen = 'SELECT pegawai.id, pegawai.nip, pegawai.nama, pegawai.pangkat, pegawai.jabatan, ' +
      'data_absen.tanggal, data_absen.waktu ' +
      'FROM pegawai JOIN data_absen ON pegawai.id=data_absen.id_pegawai ' +
      `WHERE tanggal<'${tahun}-${nextBulan}-01' AND tanggal>='${tahun}-${bulan}-01' AND id_absensi=?`;
    const queryIzin = 'SELECT pegawai.id, pegawai.nip, pegawai.nama, pegawai.pangkat, pegawai.jabatan, ' +
      'perizinan.tanggal, perizinan.keterangan ' +
      'FROM pegawai JOIN perizinan ON pegawai.id=perizinan.id_pegawai ' +
      `WHERE tanggal<'${tahun}-${nextBulan}-01' AND tanggal>='${tahun}-${bulan}-01' AND id_absensi=?`;
    const absen = await execQuery(queryAbsen, [absensi]);
    const izin = await execQuery(queryIzin, [absensi]);
    const results = absen.results.concat(izin.results);

    const laporan = [];

    results.forEach(absen => {
      const {id, nip, nama, pangkat, jabatan} = absen;
      const {tanggal, waktu, keterangan} = absen;
      
      const pegawai = {id, nip, nama, pangkat, jabatan, data_absen: []};
      const dataAbsen = {tanggal, waktu, keterangan};

      const absenSebelumnya = laporan.find(pegawai => pegawai.id === id);

      if (absenSebelumnya) {
        absenSebelumnya.data_absen.push(dataAbsen);
      } else {
        laporan.push(pegawai);
        pegawai.data_absen.push(dataAbsen);
      }
    });

    res.json(laporan);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
