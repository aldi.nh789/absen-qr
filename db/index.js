const mysql = require('mysql2');
const config = require('./config');
const init = require('./init');

const connection = mysql.createConnection(config);

function execQuery(sql, values) {
  return new Promise((resolve, reject) => {
    connection.execute(sql, values, (err, results, fields) => {
      if (err) {
        reject(err);
      } else {
        resolve({results, fields});
      }
    });
  });
}

init(execQuery)

module.exports = {
  execQuery,
};
