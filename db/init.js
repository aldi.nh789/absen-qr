module.exports = async function(execQuery) {
  const cabangQuery = 'CREATE TABLE IF NOT EXISTS cabang ' + 
    '(id INT NOT NULL AUTO_INCREMENT, nama VARCHAR(100) NOT NULL, PRIMARY KEY (id))';
  const pegawaiQuery = 'CREATE TABLE IF NOT EXISTS pegawai ' + 
    '(id INT NOT NULL AUTO_INCREMENT, nip VARCHAR(30), nama VARCHAR(100) NOT NULL, ' +
    'pangkat varchar(30), jabatan varchar(30), PRIMARY KEY (id))';
  const absensiQuery = 'CREATE TABLE IF NOT EXISTS absensi' + 
    '(id INT NOT NULL AUTO_INCREMENT, jenis VARCHAR(100), keterangan VARCHAR(30), ' + 
    'id_cabang INT NOT NULL, PRIMARY KEY (id))';
  const dataAbsenQuery = 'CREATE TABLE IF NOT EXISTS data_absen ' + 
    '(id INT NOT NULL AUTO_INCREMENT, tanggal DATE NOT NULL, waktu TIME NOT NULL, ' + 
    'id_pegawai INT NOT NULL, id_absensi INT NOT NULL, PRIMARY KEY (id))';
  const hariLiburQuery = 'CREATE TABLE IF NOT EXISTS hari_libur ' +
    '(id INT NOT NULL AUTO_INCREMENT, tanggal DATE NOT NULL, keterangan VARCHAR(30), ' +
    'PRIMARY KEY (id))'
  const perizinanQuery = 'CREATE TABLE IF NOT EXISTS perizinan ' +
    '(id INT NOT NULL AUTO_INCREMENT, tanggal DATE NOT NULL, keterangan VARCHAR(30), ' +
    'id_pegawai INT NOT NULL, id_absensi INT NOT NULL, PRIMARY KEY (id))';
  const adminQuery = 'CREATE TABLE IF NOT EXISTS admin ' +
    '(id INT NOT NULL AUTO_INCREMENT, username VARCHAR(30) NOT NULL, password VARCHAR(30) NOT NULL, PRIMARY KEY (id))';

  execQuery(pegawaiQuery).catch(console.log);
  execQuery(cabangQuery).catch(console.log);
  execQuery(absensiQuery).catch(console.log);
  execQuery(dataAbsenQuery).catch(console.log);
  execQuery(hariLiburQuery).catch(console.log);
  execQuery(perizinanQuery).catch(console.log);
  execQuery(adminQuery).catch(console.log)
    .then(_ => execQuery('SELECT * FROM admin'))
    .then(admins => {
      const {results} = admins;
      const [admin] = results;
      if (!admin) {
        return execQuery('INSERT INTO admin VALUES(null, "admin", "root")');
      }
    })
    .catch(console.log)
};
